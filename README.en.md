# 微信小程序使用本地缓存加购物车

#### Description
微信小程序使用本地缓存加购物车

#### Software Architecture
![输入图片说明](https://images.gitee.com/uploads/images/2019/0727/180257_3b598433_1561457.png "万能五笔截图_20190727180202.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0727/180313_8ea7a6c8_1561457.png "万能五笔截图_20190727180218.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0727/180327_421342e0_1561457.png "万能五笔截图_20190727180233.png")
#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)