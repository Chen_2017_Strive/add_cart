//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    swiperArry: [{
        goodsId: 1,
        oldprice: 2.1,
        text: "香嫩榴莲",
        price: 2.5,
        sale_num: 454,
        all_num: 1,
        num: 0,
        isSelect: false
      },
      {
        goodsId: 2,
        oldprice: 2.1,
        text: "小甜甜西红柿",
        price: 2.8,
        sale_num: 454,
        all_num: 7474,
        num: 0,
        isSelect: false
      },
      {
        goodsId: 3,
        oldprice: 2.1,
        text: "金银剔透龙眼",
        price: 5.1,
        sale_num: 454,
        all_num: 7474,
        num: 0,
        isSelect: false
      },
      {
        goodsId: 4,
        oldprice: 2.1,
        text: "甜蜜青梨",
        price: 5.6,
        sale_num: 454,
        all_num: 7474,
        num: 0,
        isSelect: false
      },
      {
        goodsId: 5,
        oldprice: 2.1,
        text: "小白香瓜",
        price: 15.8,
        sale_num: 454,
        all_num: 7474,
        num: 0,
        isSelect: false
      },
      {
        goodsId: 6,
        oldprice: 2.1,
        text: "奶香木瓜",
        price: 15.7,
        sale_num: 454,
        all_num: 7474,
        num: 0,
        isSelect: false
      }
    ],
    carts: [],
    num:0,
    pay:0,
  },

  //加
  addCart: function(e) {
    var _this = this;
    var index = e.currentTarget.dataset.index; //下标
    var arr = _this.data.swiperArry; //原数组
    var list = arr[index]; //取想要加入的一行
    var id = list['goodsId']; //取商品id 唯一 用来做判断 是不是加有过
    var price = list['price'];
    var all_num = list['all_num'];

    var cart = wx.getStorageSync('cart'); //获取看看有没有缓存

    if (cart == '') { //空就执行写入缓存
      list['num'] = 1;
      var data = new Array(); //定义一个空的数组
      data.push({
        gid: id,
        price: price,
        num: 1
      });

      arr[index] = list;
      _this.setData({
        carts: data,
        swiperArry: arr,
        num: 1,
        pay: price,
      })

      wx.setStorageSync('cart', JSON.stringify(data)); //写入缓存
      wx.setStorageSync('countNum', 1); //写入缓存
      wx.setStorageSync('countPay', price); //写入缓存
    } else {

      var carts = JSON.parse(cart);

      var ifjia = 0;
      var ifnum = 0;
      for (var i = 0; i < carts.length; i++) {
        var gid = carts[i]['gid'];
        if (gid == id) { //相同商品id就加1
          ifjia = 1;
          var nums = carts[i]['num']
          if (nums + 1 > all_num) { //静态库存判断
            ifnum = 1;
            break;
          }
          carts[i]['num'] += 1;
          list['num'] = carts[i]['num'];
        }
      }

      if (ifjia == 0) {
        list['num'] = 1;
        carts.push({
          gid: id,
          price: price,
          num: 1
        });
      }
      
      if (ifnum == 1){
        return;
      }

      arr[index] = list;
      _this.setData({
        carts: carts,
        swiperArry: arr
      })

      //执行统计数量及总金额计算
      _this.countCart();
    }

  },

  //减
  recalCart: function(e) {
    var _this = this;
    var index = e.currentTarget.dataset.index; //下标
    var num = e.currentTarget.dataset.num;
    var arr = _this.data.swiperArry; //原数组
    var list = arr[index]; //取想要加入的一行
    var id = list['goodsId']; //取商品id 唯一 用来做判断 是不是加有过

    var cart = wx.getStorageSync('cart'); //获取看看有没有缓存

    //购物车是否为空，数量是否为0 如果是就返回不执行下一步
    if (cart == '' || num == 0) {
      return;
    }

    //获取缓存购物车
    var carts = JSON.parse(cart);

    for (var i = 0; i < carts.length; i++) {
      var gid = carts[i]['gid'];
      if (gid == id) { //相同商品id就减1
        if (carts[i]['num'] == 1) {
          carts.splice(i, 1); //从数组下标移除
          list['num'] = 0;
        } else {
          carts[i]['num'] -= 1;
          list['num'] = carts[i]['num'];
        }
      }
    }

    arr[index] = list;
    _this.setData({
      carts: carts,
      swiperArry: arr
    })

    //执行统计数量及总金额计算
    _this.countCart();
  },

  countCart: function() {

    var _this = this;
    var carts = _this.data.carts;
    var countNum = 0;
    var countPay = 0;
    console.log(carts, '缓存数组');
    for (var i = 0; i < carts.length; i++) {
      var price = (carts[i]['price'] * 100) * carts[i]['num'];
      countNum += carts[i]['num'];
      countPay += price;
    }
    
    var pay = (countPay / 100);
    _this.setData({
      num: countNum,
      pay: pay
    })

    //写入缓存
    wx.setStorageSync('cart', JSON.stringify(carts));
    wx.setStorageSync('countNum', countNum);
    wx.setStorageSync('countPay', pay);
  },

})